using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectorColisiones : MonoBehaviour
{
    public AudioSource Source;
    public AudioClip SoundFX;
    public GameObject Object;
    public void OnGetBattery()
    {
        gameObject.SetActive(false);
        Source.transform.position = transform.position;
        Source.PlayOneShot(SoundFX);
    }
}
