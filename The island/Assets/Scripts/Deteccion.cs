using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Deteccion : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("Se entro al trigger");
    }

    private void OnTriggerStay(Collider other)
    {
        Debug.Log("Se permanece en trigger");
    }

    private void OnTriggerExit(Collider other)
    {
        Debug.Log("Se salio del trigger");
    }
}
