using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MovementController))]
public class EnemyController : MonoBehaviour
{
    public MovementController movement;
    public Transform jugador;
    public float rangeofDetection = 15f;

    private void Update()
    {
        if(jugador)
        {
            float distance = Vector3.Distance(transform.position, jugador.position);
            if (distance <= rangeofDetection)
            {
                Vector3 forward = transform.TransformDirection(Vector3.forward);
                Vector3 player = (transform.position - jugador.position).normalized;
                
                
                if (Vector3.Dot(player, forward) < 0 )
                {
                    Debug.Log("Jugador en frente");
                    movement.Rotate(1);
                    
                }
                if (Vector3.Dot(player,forward) > 0 )
                {
                    Debug.Log("Jugador detras");
                    movement.Rotate(-1);
                }


                
            }
        }
       
       
    }
}
