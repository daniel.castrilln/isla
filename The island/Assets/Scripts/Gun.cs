using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : MonoBehaviour
{
    public GameObject prefab_Bala;
    public Transform zonaDisparo;
    public float Localforce;
    public int Cartucho = 5;
    public int MaxAmmo;

    private void Start()
    {
        Cartucho = MaxAmmo;
    }
    public void Update()
    {

        if (Input.GetButtonDown("Fire1") && Cartucho > 0) 
        {
            GameObject Go =Instantiate(prefab_Bala, zonaDisparo.position, zonaDisparo.rotation);
            Go.GetComponent<Rigidbody>().AddForce(Go.transform.forward * Localforce , ForceMode.Impulse);
            Destroy(Go, 2f);
            Cartucho--;
        }

        if (Input.GetButtonDown("R"))
        {
            Reload();
        }
    }
 
    void Reload()
    {
        Cartucho = MaxAmmo;
    }
}
