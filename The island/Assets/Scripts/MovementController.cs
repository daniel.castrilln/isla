using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementController : MonoBehaviour
{
    [Header("Movimiento del Personaje")]

    public float SpeedMovement;
    public Vector3 Direccion;
    public CharacterController controller;

    [Header("Rotacion ")]

    public float rotacionPlayerY;
    /*public Camera PlayerCamera;
    private Vector2 Mouse_Movement;
    public float Rotation_CameraX;
    public float Rotation_PersonajeY;
    public float SencibilidaddelRaton;*/

    [Header("Salto del Personaje")]
    public Vector3 MovementY;
    public float gravity = -9.8f;
    public float JumpHeight;



    private void Start()
    {
        //Cursor.lockState = CursorLockMode.Locked
    }

    private void Update()
    {
        /* Calcular gravedad en cada frame */
        MovementY.y += gravity * Time.deltaTime;

        /* Mover el personaje en "Y" en base a la gravedad calculada */
        controller.Move(MovementY * Time.deltaTime);

        /* Si esta tocando el suelo y el movimiento en Y es negativo resetear la gravedad */
        if (controller.isGrounded && MovementY.y < 0)
        {
            MovementY.y = -2f;
        }

        /* Si esta tocando el suelo y al precionar la tecla, calcular el salto del personaje */
        if (controller.isGrounded && Input.GetButton("Jump"))
        {
            MovementY.y = Mathf.Sqrt(JumpHeight * -2 * gravity);
        }

    }
    public void Move(float vertical, float horizontal)
    {
        //Captura los valores del analogo del mouse
        Direccion.x = horizontal;
        Direccion.z = vertical;

        //transformar la direccion a coordenadas del jugador 
        Direccion = transform.TransformDirection(Direccion);

        //Mover al jugador en base a los inputs 
        controller.Move(Direccion* Time.deltaTime* SpeedMovement);

    }

    public void Rotate(float rotatevalue)
    {
        rotacionPlayerY += rotatevalue;
        controller.transform.rotation = Quaternion.Euler(0, rotacionPlayerY, 0);

    }

/*      /* Captura el movimiento del mouse 
   Mouse_Movement = new Vector2(Input.GetAxis("Mouse X") * SencibilidaddelRaton, Input.GetAxis("Mouse Y") * SencibilidaddelRaton);






    /* Capturar el moviento del mouse 
    Mouse_Movement.x = Input.GetAxis("Mouse X");
        Mouse_Movement.y = Input.GetAxis("Mouse Y");

        /* Almacenar movimiento del mouse 
        Rotation_CameraX -= Mouse_Movement.y;
        Rotation_PersonajeY += Mouse_Movement.x;

        /* Limitar la rotacion de la camara en el eje X 
        Rotation_CameraX = Mathf.Clamp(Rotation_CameraX, -40, 40);

        /* Rotar la camara del personaje en base al movimiento acumulado 
        PlayerCamera.transform.localRotation = Quaternion.Euler(Rotation_CameraX, 0, 0);*/

        
    }
