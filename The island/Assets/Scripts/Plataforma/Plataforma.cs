using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Plataforma : MonoBehaviour
{
    public GameObject plataform;

    public Transform minposition;
    public Transform maxposition;

    public float speed_movement;

    private void OnTriggerStay(Collider other)
    {
        if (other != null)
        {
            MovePlatform();
        }
    }

    private void MovePlatform()
    {
        plataform.transform.Translate(Vector3.up * Time.deltaTime * speed_movement);
        
        if(plataform.transform.position.y >= maxposition.position.y && speed_movement > 0)
        {
            speed_movement = speed_movement * -1f;
        }

        if (plataform.transform.position.y <= minposition.position.y && speed_movement < 0)
        {
            speed_movement = speed_movement * -1f;
        }
    }

}
