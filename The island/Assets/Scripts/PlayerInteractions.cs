using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInteractions : MonoBehaviour
{
    public Transform CameraPlayer;
    public Transform ObjetoVacio;
    public LayerMask lm;
    //public GameObject Arma;
    public Transform ObjetoVacioArma;
    public float rayDistance;

    private void Update()
    {
        if(Input.GetButtonDown("e"))
        {
            if (ObjetoVacio.childCount > 0)
            {
                ObjetoVacio.GetComponentInChildren<Rigidbody>().isKinematic = false;
                ObjetoVacio.DetachChildren();
                if (ObjetoVacioArma.childCount > 0)
                {
                    ObjetoVacioArma.GetChild(0).gameObject.SetActive(true);
                }
            }
            else
            {
                Debug.DrawRay(CameraPlayer.position, CameraPlayer.forward * rayDistance, Color.green);
                if (Physics.Raycast(CameraPlayer.position, CameraPlayer.forward, out RaycastHit hit, rayDistance, lm))
                {
                    hit.transform.GetComponent<Rigidbody>().isKinematic = true;
                    hit.transform.parent = ObjetoVacio;
                    hit.transform.localPosition = Vector3.zero;
                    hit.transform.localRotation = Quaternion.identity;
                    if (ObjetoVacioArma.childCount >0)
                    {
                        ObjetoVacioArma.GetChild(0).gameObject.SetActive(false);
                    }
                }
            }

        }

    }

    public PlayerStadistics playerStadistics;
    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Door" && playerStadistics.Battery_Count >= 3)
        {
            other.GetComponentInParent<Door>().OnOpenDoor();
        }

        if(other.tag == "Battery")
        {
            other.GetComponent<DetectorColisiones>().OnGetBattery();
            playerStadistics.Battery_Count++;
        }

        if(other.tag == "Gun")
        {
            other.transform.parent = ObjetoVacioArma;
            other.transform.localRotation = Quaternion.identity; //Quaternion.EulerAngles(new Vector3(0, 1, 0));
            other.transform.localPosition = Vector3.zero;

            if (ObjetoVacio.childCount < 0)
            {
                other.gameObject.SetActive(false);
            }


        }


    }
}
