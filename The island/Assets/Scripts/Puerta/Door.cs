using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{
    public Animator Anim;

    public void OnOpenDoor()
    {
        Anim.SetTrigger ("OpenDoor3");
    }

}
